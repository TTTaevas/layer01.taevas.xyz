# layer01.taevas.xyz

[layer01.taevas.xyz](https://layer01.taevas.xyz) is a snapshot of the state of taevas.xyz/layer01 in May 2022, which was a part of my website which was dedicated to an osu! tournament which ended 2 months prior

The snapshot was made so its content could be hosted statically (and so, inexpensively) while I was shutting down taevas.xyz mainly for money reasons
